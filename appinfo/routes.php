<?php
/**
 * ownCloud - projectapp
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Stian Dalviken <sd196@hw.ac.uk>
 * @copyright Stian Dalviken 2014
 */

$this->create('projectapp_index', '/')->action(
    function($params){
        require __DIR__ . '/../index.php';
    }
);
