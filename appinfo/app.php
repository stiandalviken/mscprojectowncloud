<?php
/**
 * ownCloud - projectapp
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Stian Dalviken <sd196@hw.ac.uk>
 * @copyright Stian Dalviken 2014
 */

\OCP\App::addNavigationEntry(array(

    // the string under which your app will be referenced in owncloud
    'id' => 'projectapp',

    // sorting weight for the navigation. The higher the number, the higher
    // will it be listed in the navigation
    'order' => 74,

    // the route that will be shown on startup
    'href' => \OCP\Util::linkToRoute('projectapp_index'),

    // the icon that will be shown in the navigation
    // this file needs to exist in img/example.png
    'icon' => \OCP\Util::imagePath('projectapp', 'calendar.png'),

    // the title of your application. This will be used in the
    // navigation or on the settings page of your app
    'name' => 'Project App'
));
