<?php
include 'functions.php';
include 'calendar.php';
include 'formRequests.php';

$calendar = new Calendar();

echo $calendar->show();

// Will only show the calAdministration div if the user is admin
if(checkIfAdmin()) {
    echo "<div id='calAdministration'>";
        echo "<h2 id='calAdministrationTitle'>Calendar administration</h2>";
        include('insertRoom.php');
        include('deleteRoom.php');
        include('insertForm.php');
    echo "</div>";
}

echo "<br class='clearfix' />";

echo "<div id='administrationMessages'>";
echo $administrationMessages;
echo "</div>";

// Prints the events based on the day selected, taken from a GET post (in the URL)
$daySelected = $_GET['year'] ."-". $_GET['month'] ."-". $_GET['day'];
echo printEvents($daySelected);

?>
