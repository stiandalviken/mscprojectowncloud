<?php 
// This file sets up the content of the Room information popup

$sql = 'SELECT * FROM `*PREFIX*projectapp_calendar_rooms` WHERE id = "'.$_GET["roomId"].'"';

$args = array(1);

$query = \ocp\db::prepare($sql);
$result = $query->execute($args);

$row = $result->fetchrow();
$roomname = $row['roomname'];
$roomtype = $row['type'];
$roomspaces = $row['spaces'];
$roomfeatures = $row['features'];
?>

<html>
<head>
<LINK href="../css/styles.css" rel="stylesheet" type="text/css">
<title><?php echo $roomname ?></title>
</head>
<body style="width:500px;height:150px;">

    <div class="calEntriesTable" >
    <table >

        <tr>
            <td>Room name</td>
            <td>Room type</td>
            <td>Spaces</td>
            <td>Features</td>
        </tr>

        <tr>
            <td><?php echo $roomname ?></td>
            <td><?php echo $roomtype ?></td>
            <td><?php echo $roomspaces ?></td>
            <td><?php echo $roomfeatures ?></td>
        </tr>

</table>
</div>
</body>
</html>
