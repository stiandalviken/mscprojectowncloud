<div id="insertForm">
<?php
// Checks to see if a date is selected, otherwise it asks the user to select a date
    if(!empty($_GET['year']) && !empty($_GET['month']) && !empty($_GET['day'])){

        $year = $_GET['year'];
        $month = $_GET['month'];
        $day = $_GET['day'];

        $date = "$year-$month-$day";
        $size = "92px";
    }else{
        $date = "Please select a date from the calendar";
        $size = "305px";
    }
?>

<h2 class="adminTitles">Book room</h2>
<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="insertFormDiv">
Date: <input id="formDate" type="text" name="date" style="width:<?php echo $size ?>;" value="<?php echo $date ?>" readonly> <br />
<p id="insertStartTime">Start time: <?php getStartTime(); ?> </p>
<p id="insertEndTime">End time: <?php getEndTime(); ?> </p> <br />
Description: <input type="text" id="insertDescription" name="description" required> <br />
Room: <?php getRoomsDropdownList(); ?> <br />
<?php getRecurrancyOptions(); ?>

<input id="submitButton" type="submit" name="insert" value="Submit">
</form>
</div>
