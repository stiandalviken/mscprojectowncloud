<div id="insertRoom">
<h2 class="adminTitles">Add new room to database</h2>
<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="insertRoomForm">
<p>Room name: <input type="text" name="roomName" id="insertRoomName" required>
Spaces: <input type="number" name="roomSpaces" id="insertRoomSpaces" min="0" required></p>
<p id="insertRoomFeaturesText">Features: </p><textarea rows="3" name="roomFeatures" id="insertRoomFeatures" required></textarea>
<p id="insertRoomType">Room type: 
<select name="roomType">
<option value="Lab">Lab</option>
<option value="Lecture">Lecture</option>
<option value="Office">Office</option>
</select>
<br class="cleafix" />
<input id="submitButtonInsert" type="submit" name="insertRoom" value="Insert">
</form>


<?php
// If the rooms database is empty, it will add a button to easily add five 
// default rooms with default attributes
if(roomsDbIsEmpty()){ 
?>
<form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" id="insertDefaultRoomForm">
<input type="submit" name="insertDefaultRoom" value="Add five default rooms">
</form>
<?php } ?>

</div>
