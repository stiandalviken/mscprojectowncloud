<?php

$roomID = $_POST["roomID"];

// Handles the POST form requests for booking a room
if ($_POST['insert'] == "Submit"){
    $username = OC_User::getUser();
    $date = $_POST["date"];
    $starttime = $_POST["starttimehour"].":".$_POST["starttimemin"];
    $endtime = $_POST["endtimehour"].":".$_POST["endtimemin"];
    $description = $_POST["description"];
    $startdatetime = "$date $starttime";
    $enddatetime = "$date $endtime";
    
    if(isset($_POST["chkboxRecurrancy"])) {
        $recurrancy = 1;
        $interval = $_POST["interval"];
        $dayWeekMonth = $_POST["dayWeekMonth"];
        $timesToRecur = $_POST["timesToRecur"];
    }

    if(!isBooked($roomID, $startdatetime, $enddatetime)){

        // If end time minus start time is more than 0, then end time is after start time, timewise
        $startdatetimesec = strtotime($startdatetime);
        $enddatetimesec = strtotime($enddatetime);
        $timediffsec = $enddatetimesec - $startdatetimesec;
        if(($timediffsec)>0){
            if ($description != "" && $roomID != ""){
                $timediffhrmin = gmdate("H:i", $timediffsec);
                insertIntoDb($username, $startdatetime, $enddatetime, $timediffhrmin, $description, $roomID, $recurrancy, $interval, $dayWeekMonth, $timesToRecur);

                $administrationMessages = '<p class="success">Message: <br />Entry successfully added to database</p>';
            }
            else{
                $administrationMessages = '<p class="error">Message: <br />Entry not added to database. Description field or room is empty, please ensure to fill these out</p>';
            }
        }

        // Check if start time and end time are set to the same
        elseif(($timediffsec)==0){
            $administrationMessages = '<p class="error">Message: <br />Entry not added to database. An event cannot be added when start time and end time are equal</p>';
        }

        // If end time is set before start time
        else{
            $administrationMessages = '<p class="error">Message: <br />Entry not added to database. An event cannot be added when end time is before start time</p>';
        }
    }
    else{
        $administrationMessages = '<p class="error">Message: <br />Entry not added to database. The proposed booking overlaps an existing booking for that room. Please choose another room, or change the start and end time of the booking</p>';
    } // If/else isBooked close
} // If INSERT close

// Handles the POST form requests for deleting an even
if ($_POST['delete'] == "Delete"){
    $id = $_POST["id"];
    if ($id != ""){
        deleteFromDb($id);
        $administrationMessages = '<p class="success">Message: <br />Event successfully deleted from database</p>';
    }
    else{
        $administrationMessages = '<p class="error">Message: <br />Event not deleted from database. Invalid event ID</p>';
    }
}

// Handles the POST form requests for adding a room
if ($_POST['insertRoom'] == "Insert"){
    if ($_POST['roomName'] != "" || $_POST['roomSpaces'] != "" || $_POST['roomFeatures'] != "" || $_POST['roomType'] != ""){
        insertRoomIntoDb($_POST['roomName'], $_POST['roomSpaces'], $_POST['roomFeatures'], $_POST['roomType']);
        $administrationMessages = '<p class="success">Message: <br />Room successfully added to database</p>';
    }
    else{
        $administrationMessages = '<p class="error">Message: <br />Room not added to database. A room name, spaces, features and/or room type is not entered</p>';
    }
}

// Handles the POST form requests for adding a room
if ($_POST['deleteRoom'] == "Delete"){
    if ($roomID != ""){
        deleteRoomFromDb($roomID);
        $administrationMessages = '<p class="success">Message: <br />Room successfully deleted from database</p>';
    }
    else{
        $administrationMessages = '<p class="error">Message: <br />Room not deleted from database. A room is not selected</p>';
    }
}

// Handles the POST request coming from the button "Add five default rooms"
if ($_POST['insertDefaultRoom'] == "Add five default rooms"){
    insertDefaultRoomsToDb();

    $administrationMessages = "<p class='success'>Message: <br />Rooms successfully added to database</p>";
}
?>
