<?php

/** Deleting row in the database based on the ID of the entry **/
function deleteFromDb($id){
    $sql = 'DELETE FROM `*PREFIX*projectapp_calendar_events` WHERE id = "'.$id.'"';

    $args = array(1);

    $query = \OCP\DB::prepare($sql);
    $query->execute($args);
}

/** Deleting room from the database based on the ID of the room **/
function deleteRoomFromDb($roomID){
    $sql = 'DELETE FROM `*PREFIX*projectapp_calendar_rooms` WHERE id = "'.$roomID.'"';

    $args = array(1);

    $query = \OCP\DB::prepare($sql);
    $query->execute($args);
}

/** Inserting events to the database **/
function insertIntoDb($username, $startdatetime, $enddatetime, $timediffhrmin, $description, $roomID, $recurrancy, $interval, $dayWeekMonth, $timesToRecur){
    $sql = 'INSERT INTO `*PREFIX*projectapp_calendar_events` (username, startdatetime, enddatetime, timediffhrmin, description, roomID) VALUES("'.$username.'","'.$startdatetime.'","'.$enddatetime.'","'.$timediffhrmin.'","'.$description.'","'.$roomID.'"); ';

    if($recurrancy == 1){
        $sql .= addRecurringEvents($username, $startdatetime, $enddatetime, $timediffhrmin, $description, $roomID, $interval, $dayWeekMonth, $timesToRecur);
    }

    $args = array(1);

    $query = \OCP\DB::prepare($sql);
    $query->execute($args);
    $query->closeCursor();
}

// Adds recurring events functionality based on user defined recurrancy; number of times to recur, day/week/month, and interval
function addRecurringEvents($username, $startdatetime, $enddatetime, $timediffhrmin, $description, $roomID, $interval, $dayWeekMonth, $timesToRecur){
    for($i =1; $i<$timesToRecur;$i++){

        $startdatetime = date('Y-m-d H:i:s', strtotime("$startdatetime + $interval $dayWeekMonth"));
        $enddatetime = date('Y-m-d H:i:s', strtotime("$enddatetime + $interval $dayWeekMonth"));

        $sql .= 'INSERT INTO `*PREFIX*projectapp_calendar_events` (username, startdatetime, enddatetime, timediffhrmin, description, roomID) ';
        $sql .= 'VALUES("'.$username.'","'.$startdatetime.'","'.$enddatetime.'","'.$timediffhrmin.'","'.$description.'","'.$roomID.'"); ';
    }
    
    return $sql;
}

/** Inserting rooms to the database **/
function insertRoomIntoDb($roomName, $roomSpaces, $roomFeatures, $roomType){
    $sql = 'INSERT INTO `*PREFIX*projectapp_calendar_rooms` (roomname, type, spaces, features) VALUES("'.$roomName.'","'.$roomType.'","'.$roomSpaces.'","'.$roomFeatures.'")';

    $args = array(1);

    $query = \OCP\DB::prepare($sql);
    $query->execute($args);
}

/** Selecting rows in the database based on the date and returning the result **/
function selectFromDb($startdatetime){
    $resultGroup = returnUsersFromGroup(); 
    if($resultGroup->rowCount() > 0){
        $usersInGroup = "";
        while($row =$resultGroup->fetchRow()){
            $username = $row['uid'];
            $usersInGroup .= ' OR username = "'.$username.'"';
        } 
    }

    $username = OC_User::getUser();
    
    //To get all entries from the whole day
    $day = explode(" ", $startdatetime);
    $startday = $day[0] . " 00:00:00";
    $endday = $day[0] . " 23:59:59";

    $sql = 'SELECT * FROM `*PREFIX*projectapp_calendar_events` WHERE startdatetime BETWEEN "'.$startday.'" AND "'.$endday.'"  AND (username = "'.$username.'" '.$usersInGroup.')';

    $args = array(1);

    $query = \OCP\DB::prepare($sql);
    $result = $query->execute($args);

    return $result;
}

// Returns result of all usernames in the groups that starts with
// "admin" (case insensitive, as it's converted to uppercase below)
function returnUsersFromGroup(){

    $adminGroupPrefix = "ADMIN";
    $sql = 'SELECT uid FROM `*PREFIX*group_user` WHERE UPPER(gid) LIKE "'.$groupName.'%"';
    
    $args = array(1);

    $query = \OCP\DB::prepare($sql);
    $result = $query->execute($args);

    return $result;
}

/** Checks if an event exists between two times, if it does, then it returnsfalse to prevent double booking **/
function isBooked($roomID, $startdatetime, $enddatetime){
    $sql = 'SELECT * FROM `*PREFIX*projectapp_calendar_events`';
    $sql.= ' WHERE roomID = "'.$roomID.'"';
    $sql.= ' AND ((startdatetime BETWEEN "'.$startdatetime.'" AND "'.$enddatetime.'")';
    $sql.= ' OR (enddatetime BETWEEN "'.$startdatetime.'" AND "'.$enddatetime.'"))';

    $args = array(1);

    $query = \OCP\DB::prepare($sql);
    $result = $query->execute($args);

    if($result->rowCount() > 0){
        return true;
    }
    else{
        return false;
    }
}

/** Creating the table for entries, and displaying the entries in the table. Only if the $result is not empty **/
function printEvents($date){
    //Returns 1 if the user is in the admin group. This way reduces the calls to 
    //the database to one, rather than checking the database for each time 
    //a table row is being printed, it stores the value in a variable
    $admin = checkIfAdmin();

    $result = selectFromDb($date);

    if($result->rowCount() > 0){
?>
            <div class="calEntriesTable" >
            <table >
            <tr>
            <?php if($admin){ echo "<td></td>"; } ?>
            <td>Added by</td>
            <td>Start time</td>
            <td>End time</td>
            <td>Duration<br />(HH:MM)</td>
            <td>Description</td>
            <td>Room</td>
            </tr>
<?php while($row = $result->fetchRow()) {
    $id = $row['id'];
    $username = $row['username'];
    $startdatetime = $row['startdatetime'];
    $enddatetime = $row['enddatetime'];
    $timediffhrmin = $row['timediffhrmin'];
    $description = $row['description'];
    $roomname = getRoomName($row['roomID']);
?>
                    <tr>

<?php //Only shows the delete button if the user is in the admin group
if($admin){ ?>
                        <td class="deleteButton"><form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <input type="submit" name="delete" value="Delete"></form></td>
                    <?php } ?>

                    <td><?php echo $username ?></td>
                    <td><?php echo $startdatetime ?></td>
                    <td><?php echo $enddatetime ?></td>
                    <td><?php echo $timediffhrmin ?></td>
                    <td class="tableDescription"><?php echo $description ?></td>
                    <td><?php echo $roomname ?></td>
                    </tr>
<?php
}
?>
            </table>
            </div>
<?php
    }
}

/** Selecting rows in the database based on the username to check if the user is in the admin group **/
function checkIfAdmin(){
    $username = OC_User::getUser();
    $sql = 'SELECT gid FROM `*PREFIX*group_user` WHERE uid = "'.$username.'"';

    $args = array(1);

    $query = \ocp\db::prepare($sql);
    $result = $query->execute($args);

    // Returns true if current user is in a group with a groupname leading with 
    // "admin" (case insensitive, as it's converted to uppercase below)
    $adminGroupPrefix = "ADMIN";
    while($row = $result->fetchrow()) { 
        $groupName = strtoupper($row['gid']);
        if(substr($groupName,0,5) === $adminGroupPrefix){
            return true;
        }
    }
}

/** In the delete form. Gets the room names from the rooms database and prints out the html code for 
 * creating the dropdown list, and dynamically populates the list with the rooms database entries **/
function getRoomsDropdownListDelete(){
    $sql = 'SELECT * FROM `*PREFIX*projectapp_calendar_rooms`';

    $args = array(1);

    $query = \ocp\db::prepare($sql);
    $result = $query->execute($args);

    echo "<select id=\"selectRoomIDDelete\" name=\"roomID\">";
    while($row = $result->fetchrow()) { 
        echo "<option value=" . $row['id'] . ">" . $row['roomname'] . "</option>";
    }
    echo "</select>";

    echo "<button id='roomInfoButtonDelete' type='button'>Room information</button>";
}

/** In the insert form. Gets the room names from the rooms database and prints out the html code for 
 * creating the dropdown list, and dynamically populates the list with the rooms 
 * database entries **/
function getRoomsDropdownList(){
    $sql = 'SELECT * FROM `*PREFIX*projectapp_calendar_rooms`';

    $args = array(1);

    $query = \ocp\db::prepare($sql);
    $result = $query->execute($args);

    echo "<select id=\"selectRoomID\" name=\"roomID\">";
    while($row = $result->fetchrow()) { 
        echo "<option value=" . $row['id'] . ">" . $row['roomname'] . "</option>";
    }
    echo "</select>";

    echo "<button id='roomInfoButton' type='button'>Room information</button>";
}

/** Returns the name of the room from the rooms database based on the ID that is being passed as the 
 * parameter **/
function getRoomName($roomID){
    $sql = 'SELECT roomname FROM `*PREFIX*projectapp_calendar_rooms` WHERE id = "'.$roomID.'"';

    $args = array(1);

    $query = \ocp\db::prepare($sql);
    $result = $query->execute($args);

    $row = $result->fetchrow();
    $roomname = $row['roomname'];

    return $roomname; 
}

//Prints dropdown lists with hours and minutes for bookings, start time
function getStartTime(){
    //Prints hours
    echo "<select id=\"starttimehour\" name=\"starttimehour\">";
    for($i = 0; $i <= 23; $i++){
        //Adds leading zero if it's less than two digits
        $i = sprintf("%02d",$i);

        echo "<option value=" . $i . ">" . $i . "</option>";
    }
    echo "</select>";

    //Prints minutes
    echo "<select name=\"starttimemin\">";
    for($i = 0; $i <= 59; $i++){
        //Only prints every fifth number, i.e. every fifth minute
        if($i % 5 == 0){
            //Adds leading zero if it's less than two digits
            $i = sprintf("%02d",$i);

            echo "<option value=" . $i . ">" . $i . "</option>";
        }
    }
    echo "</select>";
}

//Prints dropdown lists with hours and minutes for bookings, end time
function getEndTime(){
    //Prints hours
    echo "<select name=\"endtimehour\">";
    for($i = 0; $i <= 23; $i++){
        //Adds leading zero if it's less than two digits
        $i = sprintf("%02d",$i);

        echo "<option value=" . $i . ">" . $i . "</option>";
    }
    echo "</select>";

    //Prints minutes
    echo "<select id=\"endtimemin\" name=\"endtimemin\">";
    for($i = 0; $i <= 59; $i++){
        //Only prints every fifth number, i.e. every fifth minute
        if($i % 5 == 0){
            //Adds leading zero if it's less than two digits
            $i = sprintf("%02d",$i);

            echo "<option value=" . $i . ">" . $i . "</option>";
        }
    }
    echo "</select>";
}

// Prints the recurrancy options
function getRecurrancyOptions(){
?>
    Recurrancy? <input type="checkbox" id="chkboxRecurrancy" name="chkboxRecurrancy" value="unchecked" />

    <p id="recurrancyOptions">
    Every
    <input id="recurrancyInterval" type="number" name="interval" min="0" max="31" />

    <select id="selectDayWeekMonth" name="dayWeekMonth">
        <option value="day">day(s)</option>
        <option value="week">week(s)</option>
        <option value="month">month(s)</option>
    </select>
    , for 
    <input id="recurrancyTimesToRecur" type="number" name="timesToRecur" min="0" max="31" />
    times
    </p>

<?php
}

/** Inserting some default rooms to the database **/
function insertDefaultRoomsToDb($room1, $room2, $room3, $room4, $room5){
    // Checks again before executing query in case user does a refresh
    if(roomsDbIsEmpty()){
        $roomName1 = "EM1.47";
        $roomSpaces1 = "100";
        $roomFeatures1 = "Projector";
        $roomType1 = "Lecture";
        insertRoomIntoDb($roomName1, $roomSpaces1, $roomFeatures1, $roomType1);

        $roomName2 = "EM2.45";
        $roomSpaces2 = "30";
        $roomFeatures2 = "Computers";
        $roomType2 = "Lab";
        insertRoomIntoDb($roomName2, $roomSpaces2, $roomFeatures2, $roomType2);

        $roomName3 = "EM2.50";
        $roomSpaces3 = "70";
        $roomFeatures3 = "Computers, printers";
        $roomType3 = "Lab";
        insertRoomIntoDb($roomName3, $roomSpaces3, $roomFeatures3, $roomType3);

        $roomName4 = "EM3.44";
        $roomSpaces4 = "45";
        $roomFeatures4 = "2xprojectors, writing board";
        $roomType4 = "Lecture";
        insertRoomIntoDb($roomName4, $roomSpaces4, $roomFeatures4, $roomType4);

        $roomName5 = "PG2.02";
        $roomSpaces5 = "3";
        $roomFeatures5 = "None";
        $roomType5 = "Office";
        insertRoomIntoDb($roomName5, $roomSpaces5, $roomFeatures5, $roomType5);
    }
}

/** Returns true if the rooms database is empty, false otherwise **/
function roomsDbIsEmpty(){
    $sql = 'SELECT * FROM `*PREFIX*projectapp_calendar_rooms`';

    $args = array(1);

    $query = \OCP\DB::prepare($sql);
    $result = $query->execute($args);

    if($result->rowCount() == 0){
        return true;
    }
    else{
        return false;
    }
}

?>
