onload=function(){
    if(document.getElementById('administrationMessages') != undefined){  
        var mydiv=document.getElementById('administrationMessages');
        
        if(!mydiv.hasChildNodes()) {
            mydiv.style.display='none';
        }
    }
    
    if(document.getElementById('calAdministration') == undefined){  
        var calendarDiv=document.getElementById('calendar');
        calendarDiv.style.margin="0 auto";
        calendarDiv.style.float='none';
    }
}

// Creating event listeners
document.addEventListener('DOMContentLoaded', function () {
    //Adds on change event listener to recurrancy options checkbox
    document.querySelector('#chkboxRecurrancy').addEventListener('change', makeRecurrancyOptionsEditable);
    document.querySelector('#roomInfoButton').addEventListener('click', roomInfoWindow);
    document.querySelector('#roomInfoButtonDelete').addEventListener('click', roomInfoDeleteWindow);
});

// Makes recurrancy options visible if checked, and makes the input fields required
function makeRecurrancyOptionsEditable(){
    var recurrancyOptions = document.getElementById('recurrancyOptions');
    var recurrancyInterval = document.getElementById('recurrancyInterval');
    var recurrancyTimesToRecur = document.getElementById('recurrancyTimesToRecur');
    
    if(document.getElementById('chkboxRecurrancy').checked){
        recurrancyOptions.style.visibility="visible";
        recurrancyInterval.required="true";
        recurrancyTimesToRecur.required="true";
    } else{
        recurrancyOptions.style.visibility="hidden";
        recurrancyInterval.required="false";
        recurrancyTimesToRecur.required="false";
    }
}

// Creates the popup window for room information, when the Room information
// button for the "book room" section is clicked
function roomInfoWindow() {
    var selected = document.getElementById("selectRoomID").value;
    window.open('templates/roomInfo.php?roomId='+selected, target='_blank', 'scrollbars=yes, resizable=no, width=550, height=260');
}

// Creates the popup window for room information, when the Room information
// button for the "delete room" sectio is clicked
function roomInfoDeleteWindow() {
    var selected = document.getElementById("selectRoomIDDelete").value;
    window.open('templates/roomInfo.php?roomId='+selected, target='_blank', 'scrollbars=yes, resizable=no, width=550, height=260');
}
