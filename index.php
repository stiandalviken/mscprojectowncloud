<?php

// Check if we are a user
\OCP\User::checkLoggedIn();
// Check if app is enabled
\OCP\App::checkAppEnabled('projectapp');

//Importing javascript file
\OCP\Util::addScript('projectapp', 'javascript');

//Importing the two stylesheets (css files)
\OCP\Util::addStyle('projectapp', 'calendar');
\OCP\Util::addStyle('projectapp', 'styles');

$tpl = new OCP\Template("projectapp", "main", "user");
$tpl->printPage();
?>
